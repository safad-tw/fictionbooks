//
//  Book.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import SwiftyJSON


class Book {
    var rank: Int
    var rankLastWeek: Int
    var weekOnList: Int
    var publisher: String
    var price: String
    var title: String
    var author: String
    var description:String
    var imageUrl:String
    
    init(json: JSON) {
        self.rank = json["rank"].intValue
        self.rankLastWeek = json["rank_last_week"].intValue
        self.weekOnList = json["weeks_on_list"].intValue
        self.publisher = json["publisher"].stringValue
        self.price = json["price"].stringValue
        self.title = json["title"].stringValue
        self.author = json["author"].stringValue
        self.description = json["description"].stringValue
        self.imageUrl = json["book_image"].stringValue
    }
    
}




