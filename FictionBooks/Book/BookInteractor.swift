//
//  BookInteractor.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation

protocol BookInteractorProtocol {
    func fetchBooks()
    func sortByRanking()
    func sortByWeeksOnList()
}

class BookInteractor: BookInteractorProtocol {
    
    var worker: BookWorkerProtocol
    var presenter:BookPresenterProtocol
    var books: [Book]
    
    init(worker: BookWorkerProtocol, presenter: BookPresenterProtocol) {
        self.worker = worker
        self.presenter = presenter
        self.books = [Book]()
    }
    
    func fetchBooks() {
        self.worker.getBooks(onSuccess: { (books) in
            self.books = books
            self.presenter.didFetch(books: books)
        }) {
            self.presenter.didFailToFetchBooks()
        }
    }
    
    func sortByRanking() {
        let sortedBooks =  self.books.sorted(by: { $0.rank < $1.rank })
        self.presenter.showSorted(books: sortedBooks)
    }
    
    func sortByWeeksOnList() {
        let sortedBooks =  self.books.sorted(by: {$0.weekOnList > $1.weekOnList})
        self.presenter.showSorted(books: sortedBooks)
    }
    
}
