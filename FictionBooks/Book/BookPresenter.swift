//
//  BookPresenter.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation

protocol BookPresenterProtocol {
    func didFetch(books: [Book])
    func didFailToFetchBooks()
    func showSorted(books: [Book])
}

class BookPresenter {
    
    weak var view: BookViewControllerProtocol?
    
    init(view:BookViewControllerProtocol) {
        self.view = view
    }
    
}

extension BookPresenter: BookPresenterProtocol {
    
    func didFetch(books: [Book]) {
        view?.showBooks(books: mapToVM(books: books))
    }
    
    func showSorted(books: [Book]) {
        view?.showSorted(books: mapToVM(books: books))
    }
    
    func didFailToFetchBooks() {
        view?.showError()
    }
    
    func mapToVM(books: [Book]) -> [BookViewModel] {
        var booksVM = [BookViewModel]()
        for book in books {
            booksVM.append(BookViewModel(name: book.title, author: book.author, imageUrl: book.imageUrl))
            
        }
        return booksVM
    }
}



