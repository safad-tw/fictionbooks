//
//  BookResponseMapper.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import SwiftyJSON

class BookResponseMapper {
    
    class func mapToBooks(_ json: JSON) -> [Book]{
        var books = [Book]()
        if let booksArray = json["results"]["books"].array {
            for bookJson in booksArray {
                let book = Book(json: bookJson)
                books.append(book)
            }
        }
        return books
    }
    
}
