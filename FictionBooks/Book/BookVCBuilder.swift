//
//  BookBuilder.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation

class BookVCBuilder {
    
    class func buildModule(on vc: BookViewControllerProtocol) {
        let worker = BookWorker(networkManager: NetworkManager())
        let presenter = BookPresenter(view: vc)
        let interactor = BookInteractor(worker: worker, presenter: presenter)
        vc.interactor = interactor
    }
    
}
