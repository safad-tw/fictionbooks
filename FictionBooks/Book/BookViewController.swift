//
//  ViewController.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import UIKit


protocol BookViewControllerProtocol: class {
    var interactor: BookInteractorProtocol? {set get}
    func showBooks(books: [BookViewModel])
    func showSorted(books: [BookViewModel])
    func showError()
}

class BookViewController: UIViewController {
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var bookTableView: UITableView!
    
    var interactor: BookInteractorProtocol?
    var books = [BookViewModel]()
    
    override func loadView() {
        super.loadView()
        BookVCBuilder.buildModule(on: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.fetchBooks()
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged(sender:)), for: .valueChanged)
        
        self.navigationItem.title = "Books";
        // Do any additional setup after loading the view.
    }
    
    @objc func segmentedControlValueChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            interactor?.sortByRanking()
        } else {
            interactor?.sortByWeeksOnList()
        }
        
    }
    
    @IBAction func sortClicked(_ sender: Any) {
        
    }
    
}

extension BookViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.books.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0.30*Double(indexPath.row), options: [.curveEaseInOut], animations: {
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let bookCell = tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell", for: indexPath) as? BookTableViewCell {
            bookCell.selectionStyle = .none
            bookCell.setData(self.books[indexPath.row])
            return bookCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension BookViewController: BookViewControllerProtocol{
    
    func load(books:[BookViewModel]) {
        self.books = books
        self.bookTableView.reloadData()
    }
    
    func showBooks(books: [BookViewModel]) {
        self.load(books: books)
        interactor?.sortByRanking()
    }
    
    func showSorted(books: [BookViewModel]) {
        self.load(books: books)
    }
    
    func showError() {
        AlertUtil.show(on: self, title: "Error in loading")
    }
}
