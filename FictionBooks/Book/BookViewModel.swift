//
//  BookViewModel.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation

struct BookViewModel {
    let name: String
    let author: String
    let imageUrl: String
}
