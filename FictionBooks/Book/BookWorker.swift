//
//  BookWorker.swift
//  FictionBooks
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import Alamofire

protocol BookWorkerProtocol {
    func getBooks(onSuccess: @escaping ([Book]) -> Void,
               onFailure: @escaping () -> Void)
}

enum BookTarget {
    case fetchBooks
}

extension BookTarget: NetworkRequest {
    var endpoint: String {
        switch self {
        case .fetchBooks:
            return "/svc/books/v3/lists/2019-09-01/combined-print-and-e-book-fiction.json"
        }
        
    }
    var parameters: Parameters {
        switch self {
        case .fetchBooks:
            if let apiKey = AppDelegate.obfuscator?.reveal(key: KEYS.apiKey) {
                 return ["api-key": apiKey]
            }
            return [:]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .fetchBooks:
            return .get
        }
    }
}

final class BookWorker: BookWorkerProtocol {
    
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getBooks(onSuccess: @escaping ([Book]) -> Void,
                  onFailure: @escaping () -> Void)
    {
        networkManager.request(BookTarget.fetchBooks) { (isSuccess, _, responseJson) in
                                                guard isSuccess == true, let json = responseJson else {
                                                    onFailure()
                                                    return
                                                }
                           
            onSuccess(BookResponseMapper.mapToBooks(json))
        }
    }
}





