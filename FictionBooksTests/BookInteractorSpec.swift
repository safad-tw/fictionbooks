//
//  BookInteractorSpec.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import Quick
import Nimble
import SwiftyJSON
import Alamofire
@testable import FictionBooks

class WorkInteractorSpec: QuickSpec {
    
    override func spec() {
        
        describe("Spec for WorkInteractor") {
            
            var bookWorker: BookWorkerMock!
            var bookPresenter: BookPresenterMock!
            var interactor: BookInteractor!
            
            beforeEach {
                
                bookPresenter = BookPresenterMock()
                bookWorker = BookWorkerMock()
                interactor = BookInteractor(worker: bookWorker, presenter: bookPresenter)
            }
            
            describe("Spec for fetch books") {
                it("Should fetch books and call presenter method when service is success") {
                    
                    bookWorker.isServiceSuccess = true
                    interactor.fetchBooks()
                    expect(bookPresenter.books).notTo(beNil())
                    expect(bookPresenter.books?.count).to(equal(15))
                    
                }
                
                
                it("Should fetch books and call presenter method when service is failed") {
                    
                    bookWorker.isServiceSuccess = false
                    interactor.fetchBooks()
                    expect(bookPresenter.books).to(beNil())
                    expect(bookPresenter.isFailedToFetchBooks).to(beTrue())
                    
                }
            }
            
            describe("Spec for sort books by rank") {
                it("Should sort books by rank and call presenter method") {
                    
                    bookWorker.isServiceSuccess = true
                    interactor.fetchBooks()
                    interactor.sortByRanking()
                    expect(bookPresenter.books).notTo(beNil())
                    expect(bookPresenter.books?.count).to(equal(15))
                    expect(bookPresenter.books?[0].rank).to(equal(1))
                    expect(bookPresenter.books?[1].rank).to(equal(2))
                    
                }
            }
            
            describe("Spec for sort books by Weeks On List") {
                it("Should sort books by weeks on list and call presenter method") {
                    
                    bookWorker.isServiceSuccess = true
                     interactor.fetchBooks()
                    interactor.sortByWeeksOnList()
                    expect(bookPresenter.books).notTo(beNil())
                    expect(bookPresenter.books?.count).to(equal(15))
                    expect(bookPresenter.books?[0].weekOnList).to(equal(49))
                    expect(bookPresenter.books?[1].weekOnList).to(equal(48))
                    
                }
            }
            
        }
        
    }
    
}

