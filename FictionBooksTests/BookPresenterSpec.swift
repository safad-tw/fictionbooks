//
//  BookPresenterSpec.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 2/1/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import Quick
import Nimble
import SwiftyJSON
import Alamofire
@testable import FictionBooks

class BookPresenterSpec: QuickSpec {
    
    override func spec() {
        
        describe("Spec for Book Presenter") {
            
            var presenter: BookPresenter!
            var view: BookViewControllerProtocolMock!
            
            beforeEach {
                view = BookViewControllerProtocolMock()
                presenter = BookPresenter(view: view)
                
            }
            
            it("should call view method when books are fetched") {
                
                let responseJson = BookWorkerMock().readResponseFrom(fileName: "Books")
                
                presenter.didFetch(books: BookResponseMapper.mapToBooks(responseJson))
                
                expect(view.viewModel).notTo(beNil())
                expect(view.viewModel?.count).to(equal(15))
                
                
            }
            
            it("should call view method when fetch books failed") {
                presenter.didFailToFetchBooks()
                expect(view.isErrorOccured).to(beTrue())
            }
            
            it("should call view method when fetch books sorted") {
                let responseJson = BookWorkerMock().readResponseFrom(fileName: "Books")
                
                presenter.showSorted(books: BookResponseMapper.mapToBooks(responseJson))
                expect(view.viewModel).notTo(beNil())
                expect(view.viewModel?.count).to(equal(15))
            }
            
        }
        
        
    }
    
    
}
