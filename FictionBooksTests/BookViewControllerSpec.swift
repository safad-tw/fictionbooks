//
//  BookViewControllerSpec.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 2/1/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import FictionBooks

class BookViewControllerSpec: QuickSpec {
    
    
    override func spec() {
        var bookViewController:BookViewController!
        var interactor: BookInteractorMock!
        
        beforeEach {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            bookViewController=storyboard.instantiateViewController(withIdentifier: "BookViewController") as? BookViewController
            interactor = BookInteractorMock()
            bookViewController.interactor = interactor
            bookViewController.beginAppearanceTransition(true, animated: false)
            bookViewController.endAppearanceTransition()
            
        }
        
        it ("Should call interactor fetch books method on view did load") {
            bookViewController.interactor = interactor
            bookViewController.viewDidLoad()
            expect(interactor.isFetchBooksCalled).to(beTrue())
        }
        
    }
    
}
