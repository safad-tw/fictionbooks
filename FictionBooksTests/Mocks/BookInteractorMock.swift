//
//  BookInteractorMock.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import SwiftyJSON
import Alamofire
@testable import FictionBooks


class BookInteractorMock: BookInteractorProtocol{
    
    var isFetchBooksCalled: Bool?
    var isSortByRankCalled: Bool?
    var isSortByWeeksCalled: Bool?
    
    func fetchBooks() {
        self.isFetchBooksCalled = true
    }
    
    func sortByRanking() {
        self.isSortByRankCalled = true
    }
    
    func sortByWeeksOnList() {
        self.isSortByWeeksCalled = true
    }
    
    
}


