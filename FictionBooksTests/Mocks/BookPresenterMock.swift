//
//  BookPresenterMock.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//
import SwiftyJSON
import Alamofire
@testable import FictionBooks


class BookPresenterMock: BookPresenterProtocol {
    
    var books:[Book]?
    var isFailedToFetchBooks:Bool?
    
    func didFetch(books: [Book]) {
        self.books = books
    }
    func didFailToFetchBooks() {
        self.isFailedToFetchBooks = true
    }
    func showSorted(books: [Book]) {
        self.books = books
    }
}

