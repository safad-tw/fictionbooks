//
//  BookViewControllerProtocolMock.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 2/1/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import Foundation
import SwiftyJSON
@testable import FictionBooks


class BookViewControllerProtocolMock: BookViewControllerProtocol {
    
    var interactor: BookInteractorProtocol?
    var viewModel:[BookViewModel]?
    var isErrorOccured:Bool?
    
    func showBooks(books: [BookViewModel]) {
        self.viewModel = books
    }
    
    func showSorted(books: [BookViewModel]) {
        self.viewModel = books
    }
    
    func showError() {
        self.isErrorOccured = true
    }
    
}
