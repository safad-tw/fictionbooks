//
//  BookWorkerMock.swift
//  FictionBooksTests
//
//  Created by Mohammad Safad on 1/31/20.
//  Copyright © 2020 lastminute. All rights reserved.
//

import SwiftyJSON
import Alamofire
@testable import FictionBooks


class BookWorkerMock: BookWorkerProtocol {
    
    var isServiceSuccess: Bool = false
    
    func getBooks(onSuccess: @escaping ([Book]) -> Void,
                  onFailure: @escaping () -> Void) {
        
        if isServiceSuccess {
            let responseJson = readResponseFrom(fileName: "Books")
            onSuccess(BookResponseMapper.mapToBooks(responseJson))
        }
        onFailure()
    }
    
    func readResponseFrom(fileName: String) -> JSON {
        let bundle = Bundle(for: type(of: self))
        if let path = bundle.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: path, options: .alwaysMapped)
                return try JSON(data: data)
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        }
        return JSON()
    }
}


