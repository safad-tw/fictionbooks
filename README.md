<h1>FictionBooks - LastMinute.com</h1>

<h3>App Architecture</h3>

Clean swift (VIP) architecture has been adopted to structure the project

<h3>Prerequisites</h3>

Need CocoaPods

<h3>Requirements</h3>

• iOS 13.0+ <br>

• Xcode 10+ <br>

• Swift 5.0 <br>

  

<h3>Dependencies</h3>

Third party frameworks and Library are managed using Cocoapods.

<h3>Installation</h3>

<pre>

Follow these steps to setup the project

•Clone the repository into a new folder in your machine;

git clone git@bitbucket.org:safad-tw/fictionbooks.git

•Install and configure the dependencies;

•Open the Command Prompt and run: pod install

•Base url can be configured in plist file.

•Support localization.

</pre>

<h3>Pods used</h3>

<pre>
pod 'Alamofire'

pod 'SwiftyJSON'

pod 'Kingfisher'

For Unit test:

pod 'Quick'

pod 'Nimble'

</pre>

<h3>Folder Structure and Architecture</h3>

<pre>

•Each Module Will have its own folder. eg -
ViewController, Presenter, Interactor, Router, Worker, Model and Utils etc each will have its own folder

• Used Quick and Nimble for BDD
• Used Obfusctor to secure API key in the application.

</pre>
<h3>Contact</h3>
Mohammad Safad
